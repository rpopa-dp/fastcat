NAME=fastcat
CC=gcc
ARGC=-Wall -Werror -Wextra --std=c99
SRC=main.c

all: $(NAME)

$(NAME): main.c
	$(CC) $(ARGC) $(SRC) -o $(NAME)

clean:
	@rm -vf $(NAME)

re: clean all

.PHONY: all clean
